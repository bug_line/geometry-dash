import os
import pygame
import json


Res = dict["block": dict[pygame.image], "sound": dict[pygame.mixer.Sound], "other": dict[pygame.image]]
Map = list[list[dict["block_type": str, "rotation": int]]]


class Player:
    def __init__(self):
        self.pos = pygame.Vector2(0, 0)
        self.jump_speed: float = 24.6589 #block par seconde
        self.speed_x = 10.3761348898 # block par seconde
        self.speed_y = 0 # Block par seconde
        self.gravity: float = 142.537 #block par seconde carré
        self.resize(camera.block_size)

    def resize(self, size: float):
        res["other"]["player"] = pygame.transform.scale(res["other"]["player"], (size, size))

    def update(self):
        dt: float = clock.get_time() / 1000.0
        self.pos.x += self.speed_x * dt
        self.speed_y += self.gravity * dt
        self.pos.y += self.speed_y * dt
        print(self.speed_y)
        print(self.pos.x, self.pos.y)

    def jump(self):
        print("test")
        self.speed_y = -self.jump_speed


def hitbox(block, x, y) -> pygame.Rect:
    if block["block_type"] == "block":
        return pygame.Rect(x, y, 1, 1)
    elif block["block_type"] == "enemy 1":
        if block["rotation"] == 0:
            return pygame.Rect(x + 0.35, y + 0.4, 0.3, 0.1)
        elif block["rotation"] == 1:
            return pygame.Rect(x + 0.35, y + 0.4, 0.3, 0.1)
        elif block["rotation"] == 2:
            return pygame.Rect(x + 0.35, y + 0.4, 0.3, 0.1)
        elif block["rotation"] == 3:
            return pygame.Rect(x + 0.35, y + 0.4, 0.3, 0.1)
    else:
        return pygame.Rect(x, y, 1, 1)


def collision(rect_a: pygame.Rect,rect_b: pygame.Rect):
    verification = 0
    if rect_a.x >= rect_b.x and rect_a.x <= rect_b.x + rect_b.width:
        verification = 2
    if rect_a.x + rect_a.width >= rect_b.x and rect_a.x + rect_a.width <= rect_b.x + rect_b.width:
        verification = 2
    if rect_a.y + rect_a.height >= rect_b.y and rect_a.y + rect_a.height <= rect_b.y + rect_b.height:
        verification += 1
    if rect_a.y >= rect_b.y and rect_a.y <= rect_b.y + rect_b.height:
        verification += 1
    if verification > 2:
        return True
    else:
        return False


def scroll_rotation(pos, scroll):
    x, y = pos
    map[x][y]["rotation"] += scroll
    map[x][y]["rotation"] %= 4


def verification_block(pos):
    x,y =pos
    if map[x][y]["block_type"] == None:
        return False
    return True


def append_block(pos, block_type_edit):
    x, y = pos
    try:
        map[x][y]["block_type"] = block_type_edit
    except IndexError:
        print("pas dans la map")


def destroy_block(pos):
    x, y = pos
    map[x][y]["block_type"] = None


def load_res() -> Res:
    directory_other = os.path.join("res" , "other")
    res = {"block": {}, "sound": {}, "other": {}}
    for file in os.listdir(directory_other):
        filename, file_extension = os.path.splitext(file)
        f = os.path.join(directory_other, file)

        if file_extension == ".png":
            other = pygame.image.load(f)
            res["other"].update({filename: other})

        else:
            raise TypeError(f'Extension {file_extension} non supporté')

    directory_block = os.path.join("res", "block")
    for file in os.listdir(directory_block):
        filename, file_extension = os.path.splitext(file)
        f = os.path.join(directory_block, file)

        if file_extension == ".png":
            block = pygame.image.load(f)
            res["block"].update({filename: block})

        else:
            raise TypeError(f'Extension {file_extension} non supporté')

    directory_sound = os.path.join("res", "sound")
    for file in os.listdir(directory_sound):
        filename, file_extension = os.path.splitext(file)
        f = os.path.join(directory_sound, file)

        if file_extension == ".mp3" or file_extension == ".wav":
            sound = pygame.mixer.Sound(f)
            res["sound"].update({filename: sound})

        else:
            raise TypeError(f'Extension {file_extension} non supporté')

    return res


def load_map(taille_x=200, taille_y=50) -> Map:
    f = open("map.malo", "r")
    str_map = f.read()
    f.close()
    if str_map:
        map: Map = json.loads(str_map)
    else:
        map: Map = []
        for y in range(taille_x):
            map.append([])
            for x in range(taille_y):
                map[y].append({"block_type": None, "rotation": 0})
    return map


def save_map():
    f = open("map.malo", "w")
    f.write(json.dumps(map))
    f.close()


class Camera:
    def __init__(self, zoom=10 , pos=pygame.Vector2(0, 0)):
        self.zoom = zoom
        self.pos = pos
        self.min_pos_y = 6/10 * zoom
        self.max_pos_y = 11/8 * zoom
        self.block_size = height / zoom

    def mouse_block_pos(self):
        x, y = pygame.mouse.get_pos()
        block_x_px = x / self.block_size
        block_y_px = y / self.block_size
        block_x = int(self.pos.x + block_x_px)
        block_y =int(self.pos.y + block_y_px)
        return block_x, block_y

    def render_hitbox(self, block, x, y):
        rect_blk = hitbox(block, x, y)
        dist = pygame.Vector2(rect_blk.x - self.pos.x, rect_blk.y - self.pos.y)
        rect_px = pygame.Rect(dist.x * self.block_size, dist.y * self.block_size, rect_blk.width * self.block_size, rect_blk.height * self.block_size)

        pygame.draw.rect(screen, (0, 150, 0, 0), rect_px)

    def render_block(self):
        for x in range(len(map)):
            for y in range(len(map[x])):
                if map[x][y]["block_type"] != None:
                    original_image = res["block"][map[x][y]["block_type"]]
                    angle = map[x][y]["rotation"] * 90
                    image = pygame.transform.rotate(original_image, angle)
                    dist = pygame.Vector2(x - self.pos.x, y - self.pos.y)
                    left = dist.x * self.block_size
                    top = dist.y * self.block_size
                    screen.blit(image, (left, top))

                    self.render_hitbox(map[x][y], x, y)



    def render_player(self):
        image = res["other"]["player"]
        dist = pygame.Vector2(player.pos.x - self.pos.x, player.pos.y - self.pos.y)
        left = dist.x * self.block_size
        top = dist.y * self.block_size
        screen.blit(image, (left, top))

    def render(self):
        self.render_block()
        self.render_player()

    def block_resize(self):
        for block_type, block_image in res["block"].items():
            res["block"][block_type] = pygame.transform.scale(block_image, (self.block_size, self.block_size))

    def update_pos(self):
        self.pos.x = player.pos.x
        dy = player.pos.y - self.pos.y
        if dy > self.max_pos_y:
            self.pos.y = dy - self.max_pos_y
        elif dy < self.min_pos_y:
            self.pos.y = self.min_pos_y - dy

pygame.init()
print(collision(pygame.Rect(200, 200, 200, 200), pygame.Rect( 180 , 180 , 20 , 20)))
width, height = 1280, 720
screen = pygame.display.set_mode((width, height))
pygame.display.set_caption("geometry dash")
running = True
edit = True
res: Res = load_res()
map: Map = load_map()
camera = Camera()
player = Player()
block_type_edit = "block"
clock = pygame.time.Clock()
 
camera.block_resize()

while running:
    clock.tick(60)
    if not edit:
        player.update()
        camera.update_pos()

    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            running = False
            if edit:
                save_map()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_e:
                edit = not edit

        if not edit:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    player.jump()

        if edit:
            if event.type == pygame.MOUSEBUTTONUP:
                if event.button == 1:
                    append_block(camera.mouse_block_pos(), block_type_edit)
                elif event.button == 3:
                    destroy_block(camera.mouse_block_pos())

            if event.type == pygame.MOUSEWHEEL:
                scroll_rotation(camera.mouse_block_pos(), event.y)

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    camera.pos.x -= 1
                elif event.key == pygame.K_RIGHT:
                    camera.pos.x += 1
                elif event.key == pygame.K_UP:
                    camera.pos.y -= 1
                elif event.key == pygame.K_DOWN:
                    camera.pos.y += 1

                if event.key == pygame.K_1:
                    block_type_edit = "block"
                elif event.key == pygame.K_2:
                    block_type_edit = "enemy 1"
                elif event.key == pygame.K_3:
                    block_type_edit = "enemy 2"


    screen.fill((0, 0, 50))

    camera.render()
    pygame.display.flip()
